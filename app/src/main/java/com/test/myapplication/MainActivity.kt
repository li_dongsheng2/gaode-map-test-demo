package com.test.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mMapView.onCreate(savedInstanceState);
       var  aMap = mMapView.getMap();
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView.onDestroy();

    }

    override fun onResume() {
        super.onResume()
        mMapView.onResume();
    }

    override fun onPause() {
        super.onPause()
        mMapView.onPause();
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mMapView.onSaveInstanceState(outState);
    }
}